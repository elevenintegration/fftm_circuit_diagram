EESchema Schematic File Version 4
LIBS:circuit_diagram-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L circuit_diagram-rescue:Conn_02x04_Top_Bottom Ti_IMU1
U 1 1 5BE2B6A8
P 1950 1450
F 0 "Ti_IMU1" H 2000 1650 50  0000 C CNN
F 1 "Bottom        Top" H 2000 1150 50  0000 C CNN
F 2 "Connectors_Molex:Molex_Microfit3_Header_02x04_Angled_43045-080x" H 1950 1450 50  0001 C CNN
F 3 "" H 1950 1450 50  0001 C CNN
	1    1950 1450
	1    0    0    -1  
$EndComp
$Comp
L circuit_diagram-rescue:Conn_02x16_Odd_Even Ti_GPIO1
U 1 1 5C0A3B53
P 2000 3050
F 0 "Ti_GPIO1" H 2050 3850 50  0000 C CNN
F 1 "TOP          BOTTOM" H 2100 2150 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_2x16_Pitch2.54mm" H 2000 3050 50  0001 C CNN
F 3 "" H 2000 3050 50  0001 C CNN
	1    2000 3050
	1    0    0    1   
$EndComp
$Comp
L circuit_diagram-rescue:BSS138 Q4
U 1 1 5C0A3EF5
P 4500 1450
F 0 "Q4" V 4700 1525 50  0000 L CNN
F 1 "BSS138" V 4700 1450 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 4700 1375 50  0001 L CIN
F 3 "" H 4500 1450 50  0001 L CNN
	1    4500 1450
	0    1    1    0   
$EndComp
$Comp
L circuit_diagram-rescue:R Rstep4
U 1 1 5C0A3F52
P 4250 1350
F 0 "Rstep4" V 4330 1350 50  0000 C CNN
F 1 "10K" V 4250 1350 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 4180 1350 50  0001 C CNN
F 3 "" H 4250 1350 50  0001 C CNN
	1    4250 1350
	1    0    0    -1  
$EndComp
$Comp
L circuit_diagram-rescue:R Rstep8
U 1 1 5C0A3FE0
P 4750 1350
F 0 "Rstep8" V 4830 1350 50  0000 C CNN
F 1 "10K" V 4750 1350 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 4680 1350 50  0001 C CNN
F 3 "" H 4750 1350 50  0001 C CNN
	1    4750 1350
	1    0    0    -1  
$EndComp
Text GLabel 4200 1550 0    60   Input ~ 0
D46
Text GLabel 4800 1550 2    60   Input ~ 0
Hall_A_5V
$Comp
L circuit_diagram-rescue:+3.3V #PWR08
U 1 1 5C0A4218
P 4250 1100
F 0 "#PWR08" H 4250 950 50  0001 C CNN
F 1 "+3.3V" H 4250 1240 50  0000 C CNN
F 2 "" H 4250 1100 50  0001 C CNN
F 3 "" H 4250 1100 50  0001 C CNN
	1    4250 1100
	1    0    0    -1  
$EndComp
$Comp
L circuit_diagram-rescue:+5V #PWR012
U 1 1 5C0A4238
P 4750 1100
F 0 "#PWR012" H 4750 950 50  0001 C CNN
F 1 "+5V" H 4750 1240 50  0000 C CNN
F 2 "" H 4750 1100 50  0001 C CNN
F 3 "" H 4750 1100 50  0001 C CNN
	1    4750 1100
	1    0    0    -1  
$EndComp
$Comp
L circuit_diagram-rescue:GND #PWR02
U 1 1 5C0A4368
P 2000 7100
F 0 "#PWR02" H 2000 6850 50  0001 C CNN
F 1 "GND" H 2000 6950 50  0000 C CNN
F 2 "" H 2000 7100 50  0001 C CNN
F 3 "" H 2000 7100 50  0001 C CNN
	1    2000 7100
	1    0    0    -1  
$EndComp
Text Label 2050 7050 0    60   ~ 0
GND
$Comp
L circuit_diagram-rescue:+3.3V #PWR01
U 1 1 5C0A454B
P 2000 6700
F 0 "#PWR01" H 2000 6550 50  0001 C CNN
F 1 "+3.3V" H 2000 6840 50  0000 C CNN
F 2 "" H 2000 6700 50  0001 C CNN
F 3 "" H 2000 6700 50  0001 C CNN
	1    2000 6700
	1    0    0    -1  
$EndComp
Text Label 2050 6800 0    60   ~ 0
3.3V
Text GLabel 1750 2350 0    60   Input ~ 0
MOSI_D75
Text GLabel 1750 2450 0    60   Input ~ 0
CS_D10
Text GLabel 1750 2650 0    60   Input ~ 0
D13_PWM
Text GLabel 1750 2750 0    60   Input ~ 0
A0
Text GLabel 1750 2950 0    60   Input ~ 0
A2
Text GLabel 2250 7050 2    60   Input ~ 0
GND
Text GLabel 2250 6800 2    60   Input ~ 0
3.3V
Text GLabel 1750 2250 0    60   Input ~ 0
GND
Text GLabel 2350 2250 2    60   Input ~ 0
3.3V
Text GLabel 1750 2550 0    60   Input ~ 0
GND
Text GLabel 2350 2550 2    60   Input ~ 0
3.3V
Text GLabel 1750 2850 0    60   Input ~ 0
GND
Text GLabel 2350 2850 2    60   Input ~ 0
3.3V
Text GLabel 1750 3050 0    60   Input ~ 0
GND
Text GLabel 2350 3050 2    60   Input ~ 0
3.3V
Text GLabel 1750 3150 0    60   Input ~ 0
SDA
Text GLabel 1750 3250 0    60   Input ~ 0
GND
Text GLabel 1750 3350 0    60   Input ~ 0
Serial4_Tx/A11
Text GLabel 1750 3450 0    60   Input ~ 0
GND
Text GLabel 1750 3550 0    60   Input ~ 0
D50
Text GLabel 1750 3650 0    60   Input ~ 0
D48
Text GLabel 1750 3750 0    60   Input ~ 0
D46
Text GLabel 2350 2350 2    60   Input ~ 0
SCK_D76
Text GLabel 2350 2450 2    60   Input ~ 0
MISO_D74
Text GLabel 2350 2650 2    60   Input ~ 0
D43
Text GLabel 2350 2750 2    60   Input ~ 0
A1
Text GLabel 2350 2950 2    60   Input ~ 0
D8_PWM
Text GLabel 2350 3150 2    60   Input ~ 0
SCL
Text GLabel 2350 3250 2    60   Input ~ 0
3.3V
Text GLabel 2350 3350 2    60   Input ~ 0
Serial4_Rx/D52
Text GLabel 2350 3450 2    60   Input ~ 0
3.3V
Text GLabel 2350 3550 2    60   Input ~ 0
D49
Text GLabel 2350 3650 2    60   Input ~ 0
D47
Text GLabel 2350 3750 2    60   Input ~ 0
D45
Text GLabel 1700 1350 0    60   Input ~ 0
SCK_D76
Text GLabel 2300 1650 2    60   Input ~ 0
MOSI_D75
Text GLabel 2300 1550 2    60   Input ~ 0
CS_D4
Text GLabel 1700 1450 0    60   Input ~ 0
MISO_D74
Text GLabel 1700 1550 0    60   Input ~ 0
3.3V
Text GLabel 1700 1650 0    60   Input ~ 0
GND
Text GLabel 2300 1450 2    60   Input ~ 0
DRDY_D42
Text GLabel 2300 1350 2    60   Input ~ 0
RST_D41
$Comp
L circuit_diagram-rescue:Conn_02x02_Counter_Clockwise MOLEX2
U 1 1 5C0A5DA3
P 2000 4700
F 0 "MOLEX2" H 2050 4800 50  0000 C CNN
F 1 "Bottom      TOP" H 2050 4500 50  0000 C CNN
F 2 "Connectors_Molex:Molex_Microfit3_Header_02x02_Angled_43045-040x" H 2000 4700 50  0001 C CNN
F 3 "" H 2000 4700 50  0001 C CNN
	1    2000 4700
	1    0    0    -1  
$EndComp
Text GLabel 1750 4800 0    60   Input ~ 0
A
Text GLabel 1750 4700 0    60   Input ~ 0
B
Text GLabel 2350 4800 2    60   Input ~ 0
GND
Text GLabel 2350 4700 2    60   Input ~ 0
Vin
$Comp
L circuit_diagram-rescue:Conn_02x02_Counter_Clockwise MOLEX1
U 1 1 5C0A60FA
P 2000 4300
F 0 "MOLEX1" H 2050 4400 50  0000 C CNN
F 1 "Bottom      TOP" H 2050 4100 50  0000 C CNN
F 2 "Connectors_Molex:Molex_Microfit3_Header_02x02_Angled_43045-040x" H 2000 4300 50  0001 C CNN
F 3 "" H 2000 4300 50  0001 C CNN
	1    2000 4300
	1    0    0    -1  
$EndComp
Text GLabel 1750 4400 0    60   Input ~ 0
A
Text GLabel 1750 4300 0    60   Input ~ 0
B
Text GLabel 2350 4400 2    60   Input ~ 0
GND
Text GLabel 2350 4300 2    60   Input ~ 0
Vin
$Comp
L circuit_diagram-rescue:Conn_01x04 J5
U 1 1 5C0A61D4
P 10500 4300
F 0 "J5" H 10500 4500 50  0000 C CNN
F 1 "Encoder" H 10500 4000 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x04_Pitch2.54mm" H 10500 4300 50  0001 C CNN
F 3 "" H 10500 4300 50  0001 C CNN
	1    10500 4300
	1    0    0    -1  
$EndComp
Text GLabel 10250 4200 0    60   Input ~ 0
10V
Text GLabel 10250 4300 0    60   Input ~ 0
GND
Text GLabel 10250 4400 0    60   Input ~ 0
TEST
Text GLabel 10250 4500 0    60   Input ~ 0
Q
Text Notes 9950 4050 0    60   ~ 0
Encoder connector\nprovide 10-15V
Text Notes 5200 850  0    60   ~ 0
Logic level shifter for the \nLinear actuator interface and\nthe Relays for the motor controller
Text Notes 1000 1150 0    60   ~ 0
IMU connector in which the ADC ADS1256 is connected\n
Text Notes 1500 2050 0    60   ~ 0
GPOI of the Ti Board
Text Notes 1400 4100 0    60   ~ 0
RS485 pass and Power connector
$Comp
L circuit_diagram-rescue:MIC5205-5.0 U1
U 1 1 5C0A66B7
P 2000 5600
F 0 "U1" H 1850 5825 50  0000 C CNN
F 1 "MIC5205-5.0" H 2000 5825 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23-5" H 2000 5925 50  0001 C CNN
F 3 "" H 2000 5600 50  0001 C CNN
	1    2000 5600
	1    0    0    -1  
$EndComp
Text Notes 1750 5250 0    60   ~ 0
5-16V regulator
Text GLabel 700  5500 0    60   Input ~ 0
Vin
Text GLabel 1350 6250 0    60   Input ~ 0
GND
$Comp
L circuit_diagram-rescue:LED PWR_LED1
U 1 1 5C0A69DE
P 2700 5700
F 0 "PWR_LED1" H 2700 5800 50  0000 C CNN
F 1 "LED" H 2700 5600 50  0000 C CNN
F 2 "LEDs:LED_0603_HandSoldering" H 2700 5700 50  0001 C CNN
F 3 "" H 2700 5700 50  0001 C CNN
	1    2700 5700
	0    -1   -1   0   
$EndComp
$Comp
L circuit_diagram-rescue:R R1
U 1 1 5C0A6B01
P 2700 6050
F 0 "R1" V 2780 6050 50  0000 C CNN
F 1 "1K" V 2700 6050 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 2630 6050 50  0001 C CNN
F 3 "" H 2700 6050 50  0001 C CNN
	1    2700 6050
	1    0    0    -1  
$EndComp
$Comp
L circuit_diagram-rescue:CP1 Ct_Ps1
U 1 1 5C0A6C4E
P 1500 5750
F 0 "Ct_Ps1" H 1525 5850 50  0000 L CNN
F 1 "10uF" H 1525 5650 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 1500 5750 50  0001 C CNN
F 3 "" H 1500 5750 50  0001 C CNN
	1    1500 5750
	1    0    0    -1  
$EndComp
$Comp
L circuit_diagram-rescue:CP1 Ct_Ps2
U 1 1 5C0A6CD9
P 2950 5700
F 0 "Ct_Ps2" H 2975 5800 50  0000 L CNN
F 1 "10uF" H 2975 5600 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 2950 5700 50  0001 C CNN
F 3 "" H 2950 5700 50  0001 C CNN
	1    2950 5700
	1    0    0    -1  
$EndComp
$Comp
L circuit_diagram-rescue:C Cc_Ps1
U 1 1 5C0A6DE0
P 3250 5700
F 0 "Cc_Ps1" H 3275 5800 50  0000 L CNN
F 1 "0.1uF" H 3275 5600 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 3288 5550 50  0001 C CNN
F 3 "" H 3250 5700 50  0001 C CNN
	1    3250 5700
	1    0    0    -1  
$EndComp
$Comp
L circuit_diagram-rescue:+5V #PWR03
U 1 1 5C0A6EED
P 3050 5400
F 0 "#PWR03" H 3050 5250 50  0001 C CNN
F 1 "+5V" H 3050 5540 50  0000 C CNN
F 2 "" H 3050 5400 50  0001 C CNN
F 3 "" H 3050 5400 50  0001 C CNN
	1    3050 5400
	1    0    0    -1  
$EndComp
Text Notes 7950 1300 0    60   ~ 0
Section for the distance sensor
Text Notes 9750 1700 0    60   ~ 0
Section for the linear actuator
Text Notes 9850 2650 0    60   ~ 0
Section for the motor controller\n
Text GLabel 10250 2850 0    60   Input ~ 0
MOT_START
Text GLabel 10250 2950 0    60   Input ~ 0
MOT_REVERSE
Text GLabel 10250 3150 0    60   Input ~ 0
MOT_JOG
Text GLabel 10250 3050 0    60   Input ~ 0
MOT_RESET
Text GLabel 10250 3250 0    60   Input ~ 0
GND
Text GLabel 10250 2300 0    60   Input ~ 0
Hall_A_5V
Text GLabel 10250 2400 0    60   Input ~ 0
Hall_B_5V
Text GLabel 10250 1900 0    60   Input ~ 0
5V
Text GLabel 10250 2000 0    60   Input ~ 0
GND
$Comp
L circuit_diagram-rescue:Conn_01x07 J4
U 1 1 5C0AE40D
P 10500 3150
F 0 "J4" H 10500 3550 50  0000 C CNN
F 1 "MOT_CTRL" H 10500 2750 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x07_Pitch2.54mm" H 10500 3150 50  0001 C CNN
F 3 "" H 10500 3150 50  0001 C CNN
	1    10500 3150
	1    0    0    -1  
$EndComp
Text Notes 9200 3750 0    60   ~ 0
The analog in to the motor requires an OP AMP \nto control the speed via a PWM signal from the Ti
Text GLabel 10250 3350 0    60   Input ~ 0
MOT_10V
Text GLabel 10250 3450 0    60   Input ~ 0
MOT_AIN
Text GLabel 10250 2100 0    60   Input ~ 0
Linear_MOT_Enable
Text GLabel 10250 2200 0    60   Input ~ 0
Linear_MOT_Direction
Text Notes 1000 5150 0    60   ~ 0
The main Voltage supply comes from the RS485
$Comp
L circuit_diagram-rescue:Conn_01x06 J3
U 1 1 5C0F8B91
P 10500 2100
F 0 "J3" H 10500 2400 50  0000 C CNN
F 1 "Linear_Act" H 10500 1700 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x06_Pitch2.54mm" H 10500 2100 50  0001 C CNN
F 3 "" H 10500 2100 50  0001 C CNN
	1    10500 2100
	1    0    0    -1  
$EndComp
$Comp
L circuit_diagram-rescue:Conn_01x04 J1
U 1 1 5C0F9A98
P 5200 5350
F 0 "J1" H 5200 5550 50  0000 C CNN
F 1 "Temp/Humidity" H 5200 5050 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x04_Pitch2.54mm" H 5200 5350 50  0001 C CNN
F 3 "" H 5200 5350 50  0001 C CNN
	1    5200 5350
	1    0    0    -1  
$EndComp
Text Notes 4650 5050 0    60   ~ 0
temp and humidity sensor
Text GLabel 4950 5250 0    60   Input ~ 0
3.3V
Text GLabel 4950 5350 0    60   Input ~ 0
GND
Text GLabel 4950 5450 0    60   Input ~ 0
SCL
Text GLabel 4950 5550 0    60   Input ~ 0
SDA
Text GLabel 5700 7000 0    60   Input ~ 0
GND
Text GLabel 5700 6150 0    60   Input ~ 0
MOT_10V
Text GLabel 6400 6550 2    60   Input ~ 0
MOT_AIN
$Comp
L circuit_diagram-rescue:R Ramp2
U 1 1 5C0FEF7A
P 5750 7250
F 0 "Ramp2" V 5830 7250 50  0000 C CNN
F 1 "20K" V 5750 7250 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 5680 7250 50  0001 C CNN
F 3 "" H 5750 7250 50  0001 C CNN
	1    5750 7250
	0    1    1    0   
$EndComp
$Comp
L circuit_diagram-rescue:R Ramp1
U 1 1 5C0FF024
P 4900 6650
F 0 "Ramp1" V 4980 6650 50  0000 C CNN
F 1 "10K" V 4900 6650 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 4830 6650 50  0001 C CNN
F 3 "" H 4900 6650 50  0001 C CNN
	1    4900 6650
	0    -1   -1   0   
$EndComp
Text GLabel 4050 6200 0    60   Input ~ 0
D8_PWM
Text GLabel 4650 6950 0    60   Input ~ 0
GND
Text GLabel 9200 4200 2    60   Input ~ 0
Q
Text GLabel 8550 4200 0    60   Input ~ 0
D43
Text GLabel 2300 7550 2    60   Input ~ 0
10V
Text GLabel 2150 7550 0    60   Input ~ 0
Vin
$Comp
L circuit_diagram-rescue:BSS138 Q6
U 1 1 5C104CEC
P 7000 1500
F 0 "Q6" V 7200 1575 50  0000 L CNN
F 1 "BSS138" V 7200 1500 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 7200 1425 50  0001 L CIN
F 3 "" H 7000 1500 50  0001 L CNN
	1    7000 1500
	0    1    1    0   
$EndComp
$Comp
L circuit_diagram-rescue:R Rstep10
U 1 1 5C104CF2
P 6750 1400
F 0 "Rstep10" V 6830 1400 50  0000 C CNN
F 1 "10K" V 6750 1400 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 6680 1400 50  0001 C CNN
F 3 "" H 6750 1400 50  0001 C CNN
	1    6750 1400
	1    0    0    -1  
$EndComp
$Comp
L circuit_diagram-rescue:R Rstep14
U 1 1 5C104CF8
P 7250 1400
F 0 "Rstep14" V 7330 1400 50  0000 C CNN
F 1 "10K" V 7250 1400 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 7180 1400 50  0001 C CNN
F 3 "" H 7250 1400 50  0001 C CNN
	1    7250 1400
	1    0    0    -1  
$EndComp
Text GLabel 6700 1600 0    60   Input ~ 0
D45
Text GLabel 7300 1600 2    60   Input ~ 0
Hall_B_5V
$Comp
L circuit_diagram-rescue:+3.3V #PWR014
U 1 1 5C104D0A
P 6750 1150
F 0 "#PWR014" H 6750 1000 50  0001 C CNN
F 1 "+3.3V" H 6750 1290 50  0000 C CNN
F 2 "" H 6750 1150 50  0001 C CNN
F 3 "" H 6750 1150 50  0001 C CNN
	1    6750 1150
	1    0    0    -1  
$EndComp
$Comp
L circuit_diagram-rescue:+5V #PWR018
U 1 1 5C104D10
P 7250 1150
F 0 "#PWR018" H 7250 1000 50  0001 C CNN
F 1 "+5V" H 7250 1290 50  0000 C CNN
F 2 "" H 7250 1150 50  0001 C CNN
F 3 "" H 7250 1150 50  0001 C CNN
	1    7250 1150
	1    0    0    -1  
$EndComp
$Comp
L circuit_diagram-rescue:+5V #PWR04
U 1 1 5C106AA8
P 2800 6700
F 0 "#PWR04" H 2800 6550 50  0001 C CNN
F 1 "+5V" H 2800 6840 50  0000 C CNN
F 2 "" H 2800 6700 50  0001 C CNN
F 3 "" H 2800 6700 50  0001 C CNN
	1    2800 6700
	1    0    0    -1  
$EndComp
Text GLabel 2900 6800 2    60   Input ~ 0
5V
Text Notes 4650 6050 0    60   ~ 0
Op Amp for the motor speed controller
$Comp
L circuit_diagram-rescue:BSS138 Q7
U 1 1 5C108FF7
P 7000 2300
F 0 "Q7" V 7200 2375 50  0000 L CNN
F 1 "BSS138" V 7200 2300 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 7200 2225 50  0001 L CIN
F 3 "" H 7000 2300 50  0001 L CNN
	1    7000 2300
	0    1    1    0   
$EndComp
$Comp
L circuit_diagram-rescue:R Rstep11
U 1 1 5C108FFD
P 6750 2200
F 0 "Rstep11" V 6830 2200 50  0000 C CNN
F 1 "10K" V 6750 2200 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 6680 2200 50  0001 C CNN
F 3 "" H 6750 2200 50  0001 C CNN
	1    6750 2200
	1    0    0    -1  
$EndComp
$Comp
L circuit_diagram-rescue:R Rstep15
U 1 1 5C109003
P 7250 2200
F 0 "Rstep15" V 7330 2200 50  0000 C CNN
F 1 "10K" V 7250 2200 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 7180 2200 50  0001 C CNN
F 3 "" H 7250 2200 50  0001 C CNN
	1    7250 2200
	1    0    0    -1  
$EndComp
Text GLabel 4150 2400 0    60   Input ~ 0
Serial4_Rx/D52
$Comp
L circuit_diagram-rescue:+3.3V #PWR015
U 1 1 5C109015
P 6750 1950
F 0 "#PWR015" H 6750 1800 50  0001 C CNN
F 1 "+3.3V" H 6750 2090 50  0000 C CNN
F 2 "" H 6750 1950 50  0001 C CNN
F 3 "" H 6750 1950 50  0001 C CNN
	1    6750 1950
	1    0    0    -1  
$EndComp
$Comp
L circuit_diagram-rescue:+5V #PWR019
U 1 1 5C10901B
P 7250 1950
F 0 "#PWR019" H 7250 1800 50  0001 C CNN
F 1 "+5V" H 7250 2090 50  0000 C CNN
F 2 "" H 7250 1950 50  0001 C CNN
F 3 "" H 7250 1950 50  0001 C CNN
	1    7250 1950
	1    0    0    -1  
$EndComp
Text GLabel 7300 2400 2    60   Input ~ 0
Linear_MOT_Enable
$Comp
L circuit_diagram-rescue:BSS138 Q1
U 1 1 5C109731
P 4450 2300
F 0 "Q1" V 4675 2175 50  0000 L CNN
F 1 "BSS138" V 4650 2300 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 4650 2225 50  0001 L CIN
F 3 "" H 4450 2300 50  0001 L CNN
	1    4450 2300
	0    1    1    0   
$EndComp
$Comp
L circuit_diagram-rescue:R Rstep1
U 1 1 5C109737
P 4200 2200
F 0 "Rstep1" V 4280 2200 50  0000 C CNN
F 1 "10K" V 4200 2200 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 4130 2200 50  0001 C CNN
F 3 "" H 4200 2200 50  0001 C CNN
	1    4200 2200
	1    0    0    -1  
$EndComp
$Comp
L circuit_diagram-rescue:R Rstep5
U 1 1 5C10973D
P 4700 2200
F 0 "Rstep5" V 4780 2200 50  0000 C CNN
F 1 "10K" V 4700 2200 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 4630 2200 50  0001 C CNN
F 3 "" H 4700 2200 50  0001 C CNN
	1    4700 2200
	1    0    0    -1  
$EndComp
Text GLabel 6700 2400 0    60   Input ~ 0
D13_PWM
$Comp
L circuit_diagram-rescue:+3.3V #PWR05
U 1 1 5C10974E
P 4200 1950
F 0 "#PWR05" H 4200 1800 50  0001 C CNN
F 1 "+3.3V" H 4200 2090 50  0000 C CNN
F 2 "" H 4200 1950 50  0001 C CNN
F 3 "" H 4200 1950 50  0001 C CNN
	1    4200 1950
	1    0    0    -1  
$EndComp
$Comp
L circuit_diagram-rescue:+5V #PWR09
U 1 1 5C109754
P 4700 1950
F 0 "#PWR09" H 4700 1800 50  0001 C CNN
F 1 "+5V" H 4700 2090 50  0000 C CNN
F 2 "" H 4700 1950 50  0001 C CNN
F 3 "" H 4700 1950 50  0001 C CNN
	1    4700 1950
	1    0    0    -1  
$EndComp
Text GLabel 4750 2400 2    60   Input ~ 0
Linear_MOT_Direction
$Comp
L circuit_diagram-rescue:Conn_01x03 J6
U 1 1 5C10D7ED
P 10600 5550
F 0 "J6" H 10600 5750 50  0000 C CNN
F 1 "Distance_1" H 10600 5350 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x03_Pitch2.54mm" H 10600 5550 50  0001 C CNN
F 3 "" H 10600 5550 50  0001 C CNN
	1    10600 5550
	1    0    0    -1  
$EndComp
Text GLabel 10300 5450 0    60   Input ~ 0
10V
Text GLabel 10300 5550 0    60   Input ~ 0
GND
Text GLabel 9500 5650 0    60   Input ~ 0
A1
$Comp
L circuit_diagram-rescue:Conn_01x03 J2
U 1 1 5C10DD05
P 8850 5550
F 0 "J2" H 8850 5750 50  0000 C CNN
F 1 "Distance_2" H 8850 5350 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x03_Pitch2.54mm" H 8850 5550 50  0001 C CNN
F 3 "" H 8850 5550 50  0001 C CNN
	1    8850 5550
	1    0    0    -1  
$EndComp
Text GLabel 8550 5450 0    60   Input ~ 0
10V
Text GLabel 8550 5550 0    60   Input ~ 0
GND
Text GLabel 7850 5650 0    60   Input ~ 0
A0
Text Notes 7950 5250 0    60   ~ 0
SICK distance sensors, might need a voltage divier
$Comp
L circuit_diagram-rescue:R R_Filt1
U 1 1 5C11C014
P 4400 6200
F 0 "R_Filt1" V 4480 6200 50  0000 C CNN
F 1 "33K" V 4400 6200 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 4330 6200 50  0001 C CNN
F 3 "" H 4400 6200 50  0001 C CNN
	1    4400 6200
	0    1    1    0   
$EndComp
$Comp
L circuit_diagram-rescue:C Cc_Filt1
U 1 1 5C11C358
P 4650 6400
F 0 "Cc_Filt1" H 4675 6500 50  0000 L CNN
F 1 "4.7uF" H 4675 6300 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 4688 6250 50  0001 C CNN
F 3 "" H 4650 6400 50  0001 C CNN
	1    4650 6400
	1    0    0    -1  
$EndComp
Text GLabel 4400 6600 0    60   Input ~ 0
GND
Text Notes 600  2650 0    60   ~ 0
D13 pwm freq is\n1K Hz
Text Notes 650  3600 0    60   ~ 0
D8 PWM freq\nis 1.004KHz
$Comp
L circuit_diagram-rescue:R Rdiv3
U 1 1 5C128342
P 8800 4200
F 0 "Rdiv3" V 8880 4200 50  0000 C CNN
F 1 "0K" V 8800 4200 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 8730 4200 50  0001 C CNN
F 3 "" H 8800 4200 50  0001 C CNN
	1    8800 4200
	0    -1   -1   0   
$EndComp
$Comp
L circuit_diagram-rescue:R Rdiv4
U 1 1 5C1286F2
P 9000 4450
F 0 "Rdiv4" V 9080 4450 50  0000 C CNN
F 1 "0K" V 9000 4450 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 8930 4450 50  0001 C CNN
F 3 "" H 9000 4450 50  0001 C CNN
	1    9000 4450
	1    0    0    -1  
$EndComp
Text GLabel 8950 4700 0    60   Input ~ 0
GND
$Comp
L circuit_diagram-rescue:R Rdiv5
U 1 1 5C12ADFF
P 9800 5650
F 0 "Rdiv5" V 9880 5650 50  0000 C CNN
F 1 "0K" V 9800 5650 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 9730 5650 50  0001 C CNN
F 3 "" H 9800 5650 50  0001 C CNN
	1    9800 5650
	0    -1   -1   0   
$EndComp
$Comp
L circuit_diagram-rescue:R Rdiv6
U 1 1 5C12AFBF
P 10050 5850
F 0 "Rdiv6" V 10130 5850 50  0000 C CNN
F 1 "0K" V 10050 5850 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 9980 5850 50  0001 C CNN
F 3 "" H 10050 5850 50  0001 C CNN
	1    10050 5850
	1    0    0    -1  
$EndComp
Text GLabel 10000 6150 0    60   Input ~ 0
GND
$Comp
L circuit_diagram-rescue:R Rdiv1
U 1 1 5C12B7BF
P 8150 5650
F 0 "Rdiv1" V 8230 5650 50  0000 C CNN
F 1 "0K" V 8150 5650 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 8080 5650 50  0001 C CNN
F 3 "" H 8150 5650 50  0001 C CNN
	1    8150 5650
	0    -1   -1   0   
$EndComp
$Comp
L circuit_diagram-rescue:R Rdiv2
U 1 1 5C12B8B1
P 8400 5850
F 0 "Rdiv2" V 8480 5850 50  0000 C CNN
F 1 "0K" V 8400 5850 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 8330 5850 50  0001 C CNN
F 3 "" H 8400 5850 50  0001 C CNN
	1    8400 5850
	1    0    0    -1  
$EndComp
Text GLabel 8350 6150 0    60   Input ~ 0
GND
$Comp
L circuit_diagram-rescue:BSS138 Q2
U 1 1 5C1330B3
P 4450 3250
F 0 "Q2" V 4650 3325 50  0000 L CNN
F 1 "BSS138" V 4650 3250 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 4650 3175 50  0001 L CIN
F 3 "" H 4450 3250 50  0001 L CNN
	1    4450 3250
	0    1    1    0   
$EndComp
$Comp
L circuit_diagram-rescue:R Rstep2
U 1 1 5C1330B9
P 4200 3150
F 0 "Rstep2" V 4280 3150 50  0000 C CNN
F 1 "10K" V 4200 3150 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 4130 3150 50  0001 C CNN
F 3 "" H 4200 3150 50  0001 C CNN
	1    4200 3150
	1    0    0    -1  
$EndComp
$Comp
L circuit_diagram-rescue:R Rstep6
U 1 1 5C1330BF
P 4700 3150
F 0 "Rstep6" V 4780 3150 50  0000 C CNN
F 1 "10K" V 4700 3150 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 4630 3150 50  0001 C CNN
F 3 "" H 4700 3150 50  0001 C CNN
	1    4700 3150
	1    0    0    -1  
$EndComp
Text GLabel 4150 3350 0    60   Input ~ 0
D50
Text GLabel 4750 3350 2    60   Input ~ 0
MOT_REVERSE
$Comp
L circuit_diagram-rescue:+3.3V #PWR06
U 1 1 5C1330D1
P 4200 2900
F 0 "#PWR06" H 4200 2750 50  0001 C CNN
F 1 "+3.3V" H 4200 3040 50  0000 C CNN
F 2 "" H 4200 2900 50  0001 C CNN
F 3 "" H 4200 2900 50  0001 C CNN
	1    4200 2900
	1    0    0    -1  
$EndComp
$Comp
L circuit_diagram-rescue:+5V #PWR010
U 1 1 5C1330D7
P 4700 2900
F 0 "#PWR010" H 4700 2750 50  0001 C CNN
F 1 "+5V" H 4700 3040 50  0000 C CNN
F 2 "" H 4700 2900 50  0001 C CNN
F 3 "" H 4700 2900 50  0001 C CNN
	1    4700 2900
	1    0    0    -1  
$EndComp
$Comp
L circuit_diagram-rescue:BSS138 Q8
U 1 1 5C133330
P 7000 3300
F 0 "Q8" V 7200 3375 50  0000 L CNN
F 1 "BSS138" V 7200 3300 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 7200 3225 50  0001 L CIN
F 3 "" H 7000 3300 50  0001 L CNN
	1    7000 3300
	0    1    1    0   
$EndComp
$Comp
L circuit_diagram-rescue:R Rstep12
U 1 1 5C133336
P 6750 3200
F 0 "Rstep12" V 6830 3200 50  0000 C CNN
F 1 "10K" V 6750 3200 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 6680 3200 50  0001 C CNN
F 3 "" H 6750 3200 50  0001 C CNN
	1    6750 3200
	1    0    0    -1  
$EndComp
$Comp
L circuit_diagram-rescue:R Rstep16
U 1 1 5C13333C
P 7250 3200
F 0 "Rstep16" V 7330 3200 50  0000 C CNN
F 1 "10K" V 7250 3200 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 7180 3200 50  0001 C CNN
F 3 "" H 7250 3200 50  0001 C CNN
	1    7250 3200
	1    0    0    -1  
$EndComp
Text GLabel 6700 3400 0    60   Input ~ 0
D49
Text GLabel 7300 3400 2    60   Input ~ 0
MOT_START
$Comp
L circuit_diagram-rescue:+3.3V #PWR016
U 1 1 5C13334E
P 6750 2950
F 0 "#PWR016" H 6750 2800 50  0001 C CNN
F 1 "+3.3V" H 6750 3090 50  0000 C CNN
F 2 "" H 6750 2950 50  0001 C CNN
F 3 "" H 6750 2950 50  0001 C CNN
	1    6750 2950
	1    0    0    -1  
$EndComp
$Comp
L circuit_diagram-rescue:+5V #PWR020
U 1 1 5C133354
P 7250 2950
F 0 "#PWR020" H 7250 2800 50  0001 C CNN
F 1 "+5V" H 7250 3090 50  0000 C CNN
F 2 "" H 7250 2950 50  0001 C CNN
F 3 "" H 7250 2950 50  0001 C CNN
	1    7250 2950
	1    0    0    -1  
$EndComp
$Comp
L circuit_diagram-rescue:BSS138 Q3
U 1 1 5C135D11
P 4450 4050
F 0 "Q3" V 4650 4125 50  0000 L CNN
F 1 "BSS138" V 4650 4050 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 4650 3975 50  0001 L CIN
F 3 "" H 4450 4050 50  0001 L CNN
	1    4450 4050
	0    1    1    0   
$EndComp
$Comp
L circuit_diagram-rescue:R Rstep3
U 1 1 5C135D17
P 4200 3950
F 0 "Rstep3" V 4280 3950 50  0000 C CNN
F 1 "10K" V 4200 3950 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 4130 3950 50  0001 C CNN
F 3 "" H 4200 3950 50  0001 C CNN
	1    4200 3950
	1    0    0    -1  
$EndComp
$Comp
L circuit_diagram-rescue:R Rstep7
U 1 1 5C135D1D
P 4700 3950
F 0 "Rstep7" V 4780 3950 50  0000 C CNN
F 1 "10K" V 4700 3950 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 4630 3950 50  0001 C CNN
F 3 "" H 4700 3950 50  0001 C CNN
	1    4700 3950
	1    0    0    -1  
$EndComp
Text GLabel 4150 4150 0    60   Input ~ 0
D48
Text GLabel 4750 4150 2    60   Input ~ 0
MOT_RESET
$Comp
L circuit_diagram-rescue:+3.3V #PWR07
U 1 1 5C135D2F
P 4200 3700
F 0 "#PWR07" H 4200 3550 50  0001 C CNN
F 1 "+3.3V" H 4200 3840 50  0000 C CNN
F 2 "" H 4200 3700 50  0001 C CNN
F 3 "" H 4200 3700 50  0001 C CNN
	1    4200 3700
	1    0    0    -1  
$EndComp
$Comp
L circuit_diagram-rescue:+5V #PWR011
U 1 1 5C135D35
P 4700 3700
F 0 "#PWR011" H 4700 3550 50  0001 C CNN
F 1 "+5V" H 4700 3840 50  0000 C CNN
F 2 "" H 4700 3700 50  0001 C CNN
F 3 "" H 4700 3700 50  0001 C CNN
	1    4700 3700
	1    0    0    -1  
$EndComp
$Comp
L circuit_diagram-rescue:BSS138 Q5
U 1 1 5C138CF7
P 6950 4100
F 0 "Q5" V 7150 4175 50  0000 L CNN
F 1 "BSS138" V 7150 4100 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 7150 4025 50  0001 L CIN
F 3 "" H 6950 4100 50  0001 L CNN
	1    6950 4100
	0    1    1    0   
$EndComp
$Comp
L circuit_diagram-rescue:R Rstep9
U 1 1 5C138CFD
P 6700 4000
F 0 "Rstep9" V 6780 4000 50  0000 C CNN
F 1 "10K" V 6700 4000 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 6630 4000 50  0001 C CNN
F 3 "" H 6700 4000 50  0001 C CNN
	1    6700 4000
	1    0    0    -1  
$EndComp
$Comp
L circuit_diagram-rescue:R Rstep13
U 1 1 5C138D03
P 7200 4000
F 0 "Rstep13" V 7280 4000 50  0000 C CNN
F 1 "10K" V 7200 4000 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 7130 4000 50  0001 C CNN
F 3 "" H 7200 4000 50  0001 C CNN
	1    7200 4000
	1    0    0    -1  
$EndComp
Text GLabel 6650 4200 0    60   Input ~ 0
D47
Text GLabel 7250 4200 2    60   Input ~ 0
MOT_JOG
$Comp
L circuit_diagram-rescue:+3.3V #PWR013
U 1 1 5C138D15
P 6700 3750
F 0 "#PWR013" H 6700 3600 50  0001 C CNN
F 1 "+3.3V" H 6700 3890 50  0000 C CNN
F 2 "" H 6700 3750 50  0001 C CNN
F 3 "" H 6700 3750 50  0001 C CNN
	1    6700 3750
	1    0    0    -1  
$EndComp
$Comp
L circuit_diagram-rescue:+5V #PWR017
U 1 1 5C138D1B
P 7200 3750
F 0 "#PWR017" H 7200 3600 50  0001 C CNN
F 1 "+5V" H 7200 3890 50  0000 C CNN
F 2 "" H 7200 3750 50  0001 C CNN
F 3 "" H 7200 3750 50  0001 C CNN
	1    7200 3750
	1    0    0    -1  
$EndComp
$Comp
L circuit_diagram-rescue:D_Schottky MBRS2040LT3G_1
U 1 1 5C14DC32
P 1300 5500
F 0 "MBRS2040LT3G_1" H 1300 5600 50  0000 C CNN
F 1 "D_Schottky" H 1300 5400 50  0000 C CNN
F 2 "Diodes_SMD:D_SMB" H 1300 5500 50  0001 C CNN
F 3 "" H 1300 5500 50  0001 C CNN
	1    1300 5500
	-1   0    0    1   
$EndComp
$Comp
L circuit_diagram-rescue:Fuse F1
U 1 1 5C153F90
P 900 5500
F 0 "F1" V 980 5500 50  0000 C CNN
F 1 "Fuse" V 825 5500 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 830 5500 50  0001 C CNN
F 3 "" H 900 5500 50  0001 C CNN
	1    900  5500
	0    1    1    0   
$EndComp
Wire Wire Line
	4200 1550 4250 1550
Wire Wire Line
	4250 1500 4250 1550
Connection ~ 4250 1550
Wire Wire Line
	4700 1550 4750 1550
Wire Wire Line
	4750 1500 4750 1550
Connection ~ 4750 1550
Wire Wire Line
	4250 1200 4250 1150
Wire Wire Line
	4250 1150 4500 1150
Wire Wire Line
	4500 1150 4500 1250
Connection ~ 4250 1150
Wire Wire Line
	4750 1200 4750 1100
Wire Wire Line
	2000 7100 2000 7050
Wire Wire Line
	2000 7050 2250 7050
Wire Wire Line
	2000 6700 2000 6800
Wire Wire Line
	2000 6800 2250 6800
Wire Wire Line
	1800 2250 1750 2250
Wire Wire Line
	1800 2350 1750 2350
Wire Wire Line
	1800 2450 1750 2450
Wire Wire Line
	1800 2550 1750 2550
Wire Wire Line
	1800 2650 1750 2650
Wire Wire Line
	1800 2750 1750 2750
Wire Wire Line
	1800 2850 1750 2850
Wire Wire Line
	1800 2950 1750 2950
Wire Wire Line
	1800 3050 1750 3050
Wire Wire Line
	1800 3150 1750 3150
Wire Wire Line
	1800 3250 1750 3250
Wire Wire Line
	1800 3350 1750 3350
Wire Wire Line
	1800 3450 1750 3450
Wire Wire Line
	1800 3550 1750 3550
Wire Wire Line
	1800 3650 1750 3650
Wire Wire Line
	1800 3750 1750 3750
Wire Wire Line
	2300 3750 2350 3750
Wire Wire Line
	2300 3650 2350 3650
Wire Wire Line
	2300 3550 2350 3550
Wire Wire Line
	2300 3450 2350 3450
Wire Wire Line
	2300 3350 2350 3350
Wire Wire Line
	2300 3250 2350 3250
Wire Wire Line
	2300 3150 2350 3150
Wire Wire Line
	2300 3050 2350 3050
Wire Wire Line
	2300 2950 2350 2950
Wire Wire Line
	2300 2850 2350 2850
Wire Wire Line
	2300 2750 2350 2750
Wire Wire Line
	2300 2650 2350 2650
Wire Wire Line
	2300 2550 2350 2550
Wire Wire Line
	2300 2450 2350 2450
Wire Wire Line
	2300 2350 2350 2350
Wire Wire Line
	2300 2250 2350 2250
Wire Wire Line
	1750 1350 1700 1350
Wire Wire Line
	1750 1450 1700 1450
Wire Wire Line
	1750 1550 1700 1550
Wire Wire Line
	1750 1650 1700 1650
Wire Wire Line
	2250 1650 2300 1650
Wire Wire Line
	2250 1550 2300 1550
Wire Wire Line
	2250 1450 2300 1450
Wire Wire Line
	2250 1350 2300 1350
Wire Wire Line
	2350 4700 2300 4700
Wire Wire Line
	2350 4800 2300 4800
Wire Wire Line
	1800 4700 1750 4700
Wire Wire Line
	1800 4800 1750 4800
Wire Wire Line
	2350 4300 2300 4300
Wire Wire Line
	2350 4400 2300 4400
Wire Wire Line
	1800 4300 1750 4300
Wire Wire Line
	1800 4400 1750 4400
Wire Wire Line
	10250 4200 10300 4200
Wire Wire Line
	10250 4300 10300 4300
Wire Wire Line
	10250 4400 10300 4400
Wire Wire Line
	10250 4500 10300 4500
Wire Wire Line
	1550 5600 1700 5600
Wire Wire Line
	2000 5900 2000 6250
Wire Wire Line
	3050 5500 3050 5400
Wire Wire Line
	2700 5550 2700 5500
Connection ~ 2700 5500
Wire Wire Line
	2950 5550 2950 5500
Connection ~ 2950 5500
Wire Wire Line
	3250 5500 3250 5550
Connection ~ 3050 5500
Wire Wire Line
	2700 5850 2700 5900
Wire Wire Line
	1350 6250 1500 6250
Wire Wire Line
	2700 6250 2700 6200
Wire Wire Line
	1500 5900 1500 6250
Connection ~ 1500 6250
Wire Wire Line
	2950 6250 2950 5850
Connection ~ 2700 6250
Wire Wire Line
	3250 6250 3250 5850
Connection ~ 2950 6250
Wire Wire Line
	10300 2850 10250 2850
Wire Wire Line
	10300 2950 10250 2950
Wire Wire Line
	10300 3050 10250 3050
Wire Wire Line
	10300 3150 10250 3150
Wire Wire Line
	10250 3250 10300 3250
Wire Wire Line
	10250 3350 10300 3350
Wire Wire Line
	10250 3450 10300 3450
Wire Wire Line
	10250 1900 10300 1900
Wire Wire Line
	10250 2000 10300 2000
Wire Wire Line
	10250 2100 10300 2100
Wire Wire Line
	10250 2200 10300 2200
Wire Wire Line
	10250 2300 10300 2300
Wire Wire Line
	10250 2400 10300 2400
Wire Wire Line
	4950 5250 5000 5250
Wire Wire Line
	4950 5350 5000 5350
Wire Wire Line
	4950 5450 5000 5450
Wire Wire Line
	4950 5550 5000 5550
Wire Wire Line
	5700 6150 5750 6150
Wire Wire Line
	5750 6150 5750 6250
Wire Wire Line
	5750 6850 5750 7000
Wire Wire Line
	5750 7000 5700 7000
Wire Wire Line
	5050 6650 5250 6650
Wire Wire Line
	5250 6650 5250 7250
Wire Wire Line
	5250 7250 5600 7250
Connection ~ 5250 6650
Wire Wire Line
	5900 7250 6300 7250
Wire Wire Line
	6300 7250 6300 6550
Connection ~ 6300 6550
Wire Wire Line
	4700 6650 4700 6950
Wire Wire Line
	4700 6650 4750 6650
Wire Wire Line
	5200 6450 5550 6450
Wire Wire Line
	4700 6950 4650 6950
Wire Wire Line
	6150 6550 6300 6550
Wire Wire Line
	8950 4200 9000 4200
Wire Wire Line
	2150 7550 2300 7550
Wire Wire Line
	6700 1600 6750 1600
Wire Wire Line
	6750 1550 6750 1600
Connection ~ 6750 1600
Wire Wire Line
	7200 1600 7250 1600
Wire Wire Line
	7250 1550 7250 1600
Connection ~ 7250 1600
Wire Wire Line
	6750 1250 6750 1200
Wire Wire Line
	6750 1200 7000 1200
Wire Wire Line
	7000 1200 7000 1300
Connection ~ 6750 1200
Wire Wire Line
	7250 1250 7250 1150
Wire Wire Line
	2800 6700 2800 6800
Wire Wire Line
	2800 6800 2900 6800
Wire Wire Line
	6700 2400 6750 2400
Wire Wire Line
	6750 2350 6750 2400
Connection ~ 6750 2400
Wire Wire Line
	7200 2400 7250 2400
Wire Wire Line
	7250 2350 7250 2400
Connection ~ 7250 2400
Wire Wire Line
	6750 2050 6750 2000
Wire Wire Line
	6750 2000 7000 2000
Wire Wire Line
	7000 2000 7000 2100
Connection ~ 6750 2000
Wire Wire Line
	7250 2050 7250 1950
Wire Wire Line
	4150 2400 4200 2400
Wire Wire Line
	4200 2350 4200 2400
Connection ~ 4200 2400
Wire Wire Line
	4650 2400 4700 2400
Wire Wire Line
	4700 2350 4700 2400
Connection ~ 4700 2400
Wire Wire Line
	4200 2050 4200 2000
Wire Wire Line
	4200 2000 4450 2000
Wire Wire Line
	4450 2000 4450 2100
Connection ~ 4200 2000
Wire Wire Line
	4700 2050 4700 1950
Wire Wire Line
	10300 5450 10400 5450
Wire Wire Line
	10300 5550 10400 5550
Wire Wire Line
	9950 5650 10050 5650
Wire Wire Line
	8550 5450 8650 5450
Wire Wire Line
	8550 5550 8650 5550
Wire Wire Line
	8300 5650 8400 5650
Wire Wire Line
	4400 6600 4650 6600
Wire Wire Line
	4650 6600 4650 6550
Wire Wire Line
	4550 6200 4650 6200
Wire Wire Line
	4650 6200 4650 6250
Wire Wire Line
	5200 6200 5200 6450
Connection ~ 4650 6200
Wire Wire Line
	4250 6200 4050 6200
Wire Wire Line
	8550 4200 8650 4200
Wire Wire Line
	9000 4300 9000 4200
Connection ~ 9000 4200
Wire Wire Line
	9000 4600 9000 4700
Wire Wire Line
	9000 4700 8950 4700
Wire Wire Line
	9500 5650 9650 5650
Wire Wire Line
	10050 5700 10050 5650
Connection ~ 10050 5650
Wire Wire Line
	10050 6000 10050 6150
Wire Wire Line
	10050 6150 10000 6150
Wire Wire Line
	7850 5650 8000 5650
Wire Wire Line
	8400 5700 8400 5650
Connection ~ 8400 5650
Wire Wire Line
	8400 6000 8400 6150
Wire Wire Line
	8400 6150 8350 6150
Wire Wire Line
	4150 3350 4200 3350
Wire Wire Line
	4200 3300 4200 3350
Connection ~ 4200 3350
Wire Wire Line
	4650 3350 4700 3350
Wire Wire Line
	4700 3300 4700 3350
Connection ~ 4700 3350
Wire Wire Line
	4200 3000 4200 2950
Wire Wire Line
	4200 2950 4450 2950
Wire Wire Line
	4450 2950 4450 3050
Connection ~ 4200 2950
Wire Wire Line
	4700 3000 4700 2900
Wire Wire Line
	6700 3400 6750 3400
Wire Wire Line
	6750 3350 6750 3400
Connection ~ 6750 3400
Wire Wire Line
	7200 3400 7250 3400
Wire Wire Line
	7250 3350 7250 3400
Connection ~ 7250 3400
Wire Wire Line
	6750 3050 6750 3000
Wire Wire Line
	6750 3000 7000 3000
Wire Wire Line
	7000 3000 7000 3100
Connection ~ 6750 3000
Wire Wire Line
	7250 3050 7250 2950
Wire Wire Line
	4150 4150 4200 4150
Wire Wire Line
	4200 4100 4200 4150
Connection ~ 4200 4150
Wire Wire Line
	4650 4150 4700 4150
Wire Wire Line
	4700 4100 4700 4150
Connection ~ 4700 4150
Wire Wire Line
	4200 3800 4200 3750
Wire Wire Line
	4200 3750 4450 3750
Wire Wire Line
	4450 3750 4450 3850
Connection ~ 4200 3750
Wire Wire Line
	4700 3800 4700 3700
Wire Wire Line
	6650 4200 6700 4200
Wire Wire Line
	6700 4150 6700 4200
Connection ~ 6700 4200
Wire Wire Line
	7150 4200 7200 4200
Wire Wire Line
	7200 4150 7200 4200
Connection ~ 7200 4200
Wire Wire Line
	6700 3850 6700 3800
Wire Wire Line
	6700 3800 6950 3800
Wire Wire Line
	6950 3800 6950 3900
Connection ~ 6700 3800
Wire Wire Line
	7200 3850 7200 3750
Wire Wire Line
	750  5500 700  5500
Wire Wire Line
	1050 5500 1150 5500
Wire Wire Line
	1450 5500 1500 5500
Wire Wire Line
	1550 5600 1550 5500
Connection ~ 1550 5500
Wire Wire Line
	1500 5600 1500 5500
Connection ~ 1500 5500
$Comp
L circuit_diagram-rescue:OPA134 U2
U 1 1 5C166CDF
P 5850 6550
F 0 "U2" H 5900 6800 50  0000 L CNN
F 1 "MC33201DR2G" H 5900 6700 50  0000 L CNN
F 2 "Housings_SOIC:SOIC-8_3.9x4.9mm_Pitch1.27mm" H 5900 6600 50  0001 C CNN
F 3 "" H 5900 6700 50  0001 C CNN
	1    5850 6550
	1    0    0    -1  
$EndComp
$Comp
L circuit_diagram-rescue:LED PWR_LED2
U 1 1 5C113864
P 3400 7200
F 0 "PWR_LED2" H 3400 7300 50  0000 C CNN
F 1 "LED" H 3400 7100 50  0000 C CNN
F 2 "LEDs:LED_0603_HandSoldering" H 3400 7200 50  0001 C CNN
F 3 "" H 3400 7200 50  0001 C CNN
	1    3400 7200
	-1   0    0    1   
$EndComp
$Comp
L circuit_diagram-rescue:R R2
U 1 1 5C113DD4
P 3800 7200
F 0 "R2" V 3880 7200 50  0000 C CNN
F 1 "500" V 3800 7200 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 3730 7200 50  0001 C CNN
F 3 "" H 3800 7200 50  0001 C CNN
	1    3800 7200
	0    -1   -1   0   
$EndComp
Text GLabel 3150 7200 0    60   Input ~ 0
3.3V
Text GLabel 3950 7450 0    60   Input ~ 0
GND
Wire Wire Line
	3150 7200 3250 7200
Wire Wire Line
	3550 7200 3650 7200
Wire Wire Line
	3950 7200 4050 7200
Wire Wire Line
	4050 7200 4050 7450
Wire Wire Line
	4050 7450 3950 7450
Wire Wire Line
	4250 1550 4300 1550
Wire Wire Line
	4750 1550 4800 1550
Wire Wire Line
	4250 1150 4250 1100
Wire Wire Line
	2700 5500 2950 5500
Wire Wire Line
	2950 5500 3050 5500
Wire Wire Line
	3050 5500 3250 5500
Wire Wire Line
	1500 6250 2000 6250
Wire Wire Line
	2700 6250 2950 6250
Wire Wire Line
	2950 6250 3250 6250
Wire Wire Line
	5250 6650 5550 6650
Wire Wire Line
	6300 6550 6400 6550
Wire Wire Line
	6750 1600 6800 1600
Wire Wire Line
	7250 1600 7300 1600
Wire Wire Line
	6750 1200 6750 1150
Wire Wire Line
	6750 2400 6800 2400
Wire Wire Line
	7250 2400 7300 2400
Wire Wire Line
	6750 2000 6750 1950
Wire Wire Line
	4200 2400 4250 2400
Wire Wire Line
	4700 2400 4750 2400
Wire Wire Line
	4200 2000 4200 1950
Wire Wire Line
	4650 6200 5200 6200
Wire Wire Line
	9000 4200 9200 4200
Wire Wire Line
	10050 5650 10400 5650
Wire Wire Line
	8400 5650 8650 5650
Wire Wire Line
	4200 3350 4250 3350
Wire Wire Line
	4700 3350 4750 3350
Wire Wire Line
	4200 2950 4200 2900
Wire Wire Line
	6750 3400 6800 3400
Wire Wire Line
	7250 3400 7300 3400
Wire Wire Line
	6750 3000 6750 2950
Wire Wire Line
	4200 4150 4250 4150
Wire Wire Line
	4700 4150 4750 4150
Wire Wire Line
	4200 3750 4200 3700
Wire Wire Line
	6700 4200 6750 4200
Wire Wire Line
	7200 4200 7250 4200
Wire Wire Line
	6700 3800 6700 3750
Wire Wire Line
	1550 5500 1700 5500
Wire Wire Line
	1500 5500 1550 5500
Wire Wire Line
	2000 6250 2300 6250
Connection ~ 2000 6250
Wire Wire Line
	2300 5500 2700 5500
$Comp
L circuit_diagram-rescue:C Cc_Ps2
U 1 1 5C5B4755
P 2300 5975
F 0 "Cc_Ps2" H 2325 6075 50  0000 L CNN
F 1 "0.47uF" H 2325 5875 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603_HandSoldering" H 2338 5825 50  0001 C CNN
F 3 "" H 2300 5975 50  0001 C CNN
	1    2300 5975
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 5600 2300 5825
Wire Wire Line
	2300 6125 2300 6250
Connection ~ 2300 6250
Wire Wire Line
	2300 6250 2700 6250
$EndSCHEMATC
